import {addParameters, configure} from '@storybook/html';

import customTheme from './custom-theme';

addParameters({
  options: {
    theme: customTheme
  },
});

// automatically import all files ending in *.stories.js
configure(require.context('../src', true, /\.stories\.js$/), module);
