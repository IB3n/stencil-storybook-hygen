import { create } from '@storybook/theming';

export default create({
  base: 'light',

  brandTitle: 'Wing FMK',
  brandImage: '/assets/logo_text.svg',
});
