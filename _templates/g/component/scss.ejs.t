---
to: src/components/<%= h.inflection.dasherize(name) %>/<%= h.inflection.dasherize(name) %>.scss
unless_exists: true
---
app-<%= h.inflection.dasherize(name) %> {
  display: block;
}
