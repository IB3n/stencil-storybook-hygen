---
to: src/components/<%= h.inflection.dasherize(name) %>/<%= h.inflection.dasherize(name) %>.stories.js
unless_exists: true
---
export default {
  title: '<%= Name %>',
};

export const default_state = () => '<app-<%= h.inflection.dasherize(name) %>></app-<%= h.inflection.dasherize(name) %>>';
