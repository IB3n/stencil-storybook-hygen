---
to: src/components/<%= h.inflection.dasherize(name) %>/<%= h.inflection.dasherize(name) %>.tsx
unless_exists: true
---
import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'app-<%= h.inflection.dasherize(name) %>',
  styleUrl: '<%= h.inflection.dasherize(name) %>.scss',
  shadow: true
})
export class <%= Name %>Component {
  render() {
    return <div>app-<%= h.inflection.dasherize(name) %> is working</div>;
  }
}
