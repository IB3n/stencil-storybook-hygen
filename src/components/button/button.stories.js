import {addDecorator} from "@storybook/html";
import {withA11y} from "@storybook/addon-a11y";
import {withKnobs, select, text} from "@storybook/addon-knobs";


export default {
  title: 'Button',
};

addDecorator(withA11y);
addDecorator(withKnobs);

export const default_state = () => {
  const textContent = text("content", "button content", "text-content");
  const modifier = select("modifier", ["normal", "warning", "danger", "info", "success"], "normal", "select-knob");
  const appearance = select("appearance", ["raised", "stroked"], "stroked", "select-appearance");
  return `<app-button modifier="${modifier}" appearance="${appearance}">${textContent}</app-button>`;
};
