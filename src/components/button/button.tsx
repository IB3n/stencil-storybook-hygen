import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'app-button',
  styleUrl: 'button.scss',
  shadow: true
})
export class ButtonComponent {

  @Prop() modifier: "danger" | "warning" | "info" | "success" = "info";
  @Prop() appearance: "stroked" | "raised" = "stroked";

  get modifierClass() {
    return this.modifier ? ` button-${this.modifier}`: "";
  }

  get appearanceClass() {
    return this.appearance ? ` button-${this.appearance}`: "";
  }

  render() {
    return <button class={"button" + this.modifierClass + this.appearanceClass}><slot /></button>;
  }
}
