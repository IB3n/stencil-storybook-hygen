export default {
  title: 'Logo'
};

export const default_state = () => '<app-logo></app-logo>';
export const text_mode = () => '<app-logo mode="text"></app-logo>';
export const custom_color = () => '<app-logo color="hotpink"></app-logo>';
