import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'app-logo',
  styleUrl: 'logo.scss'
})
export class LogoComponent {

  @Prop() mode: "default" | "text" = "default";
  @Prop() color: string = "rebeccapurple";

  render() {
    return (<div class="logo-container" style={{color: this.color}}>
      <svg class="logo" viewBox="0 0 28.7 28.7">
        <g transform="translate(-60.5 -67.2)">
          <rect class="square" transform="rotate(58.2)" x="98.3" y="-31.1" width="20.9" height="20.9" fill="#333"/>
          <path class="rear-wing" d="m82 84.7-11.1-11.1 4.1 15.3z"/>
          <path class="front-wing" d="m68.4 77.6 6.42 11.1-2.66-8.88z"/>
        </g>
      </svg>
      {this.mode === "text" &&
        <div>
          <div class="main_name">Wing</div>
          <div class="sub_name">FMK</div>
        </div>}
    </div>);
  }
}
